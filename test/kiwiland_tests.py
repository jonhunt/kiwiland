import unittest
from lib.kiwiland_railroad_service import KiwilandRailroadService

TEST_ROUTES = [
        'AB5',
        'BC4',
        'CD8',
        'DC8',
        'DE6',
        'AD5',
        'CE2',
        'EB3',
        'AE7']

class KiwilandTests(unittest.TestCase):

    def setUp(self):
        self.kiwi_service = KiwilandRailroadService().add_routes(TEST_ROUTES)

    def test_invalid_route_codes(self):
        invalid_codes = ['a-b-9', 'CCE7', 'AD9.9']
        for route_code in invalid_codes:
            with self.assertRaises(Exception):
                self.kiwi_service.add_routes([route_code])

    def test_route_distance(self):
        for route,dist in [
                ('A-B-C', 9),
                ('A-D', 5),
                ('A-D-C', 13),
                ('A-E-B-C-D', 22),
                ('E-B-C', 7),
                ('C-E-B', 5)]:
            self.assertEqual(self.kiwi_service.get_route_distance(route), dist)
    
    def test_trip_count_w_max(self):
        trips = [
            ('C', 'C', 3, 2), # (from, to, max, expected count)
            ('A', 'C', 3, 3),
            ('A', 'E', 2, 2), 
        ]

        for src,dest,stops,expected_count in trips:
            trip_count = self.kiwi_service.get_trip_count(src, dest, max_stops=stops)
            self.assertEqual(trip_count, expected_count)

    def test_invalid_routes(self):
        invalid_routes = [
            ('A-E-D'),
            ('C-B-E'),
            ('D-E-A'),
            ('B-E-D'),
        ]

        for route in invalid_routes:
            with self.assertRaises(KiwilandRailroadService.NoSuchRouteException):
                self.kiwi_service.get_route_distance(route)

    def test_trip_count_exact(self):
        trips = [
            ('A', 'C', 4, 3), # (from, to, stops, expected count)
            ('A', 'E', 2, 1),
        ]

        for src,dest,stops,expected_count in trips:
            trip_count = self.kiwi_service.get_trip_count(src, dest, max_stops=stops,
                exact_stops=True)
            self.assertEqual(trip_count, expected_count)
                

    def test_find_shortest_route(self):
        routes = [
            ('A', 'C', 9), # from, to, dist
            ('B', 'B', 9), 
            ('E', 'D', 15),
            ('D', 'D', 16),
        ]

        for src,dest,dist in routes:
            self.assertEqual(self.kiwi_service.find_shortest_route(src, dest), dist)

    def test_find_shortest_invalid(self):
        routes = [
            ('E', 'A'),
            ('A', 'A'),
        ]

        for src,dest in routes:
            self.assertIsNone(self.kiwi_service.find_shortest_route(src,dest))

    def test_find_routes(self):
        routes = [
            ('C', 'C', 30, 7), # from, to, max distance, expected count
            ('A', 'D', 20, 2),
            ('B', 'E', 50, 38), 
        ]

        for src,dest,max_dist,expected_count in routes:
            available_routes = self.kiwi_service.find_routes(src, dest, max_distance=max_dist)
            self.assertEqual(len(available_routes), expected_count)


if __name__ == '__main__':
    unittest.main()