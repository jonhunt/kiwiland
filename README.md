# Kiwitrains Service

Kiwiland railroad service used to manage route information for the Kiwiland Railroad Company.
Allows a Kiwiland agent to find the distance of specific routes, number of available routes 
between two locations, and find the shortest route to a location.

## Usage

To run the program preloaded with the data from `doc/trains.txt`:

```shell
$ python kiwiland.py 
```

## Tests

```shell
$ python -m test.kiwiland_tests
........
----------------------------------------------------------------------
Ran 8 tests in 0.004s

OK
```