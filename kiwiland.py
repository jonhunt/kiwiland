from lib.kiwiland_railroad_service import KiwilandRailroadService

#
# Create a Kiwiland railroad service and execute with input data from 
# trains.txt when run on the command line.
#
if __name__ == '__main__':
    kiwi_service = KiwilandRailroadService().add_routes([
        'AB5',
        'BC4',
        'CD8',
        'DC8',
        'DE6',
        'AD5',
        'CE2',
        'EB3',
        'AE7'])

    # Check distance of routes
    print(kiwi_service.get_route_distance('A-B-C'))
    print(kiwi_service.get_route_distance('A-D'))
    print(kiwi_service.get_route_distance('A-D-C'))
    print(kiwi_service.get_route_distance('A-E-B-C-D'))

    try:
        print(kiwi_service.get_route_distance('A-E-D'))
    except KiwilandRailroadService.NoSuchRouteException:
        print('NO SUCH ROUTE')


    # Find number of trips
    print(kiwi_service.get_trip_count('C', 'C', max_stops=3))
    print(kiwi_service.get_trip_count('A', 'C', max_stops=4, exact_stops=True))


    # Find shortest routes
    print(kiwi_service.find_shortest_route('A', 'C'))
    print(kiwi_service.find_shortest_route('B', 'B'))

    # Find number of different routes for distance
    print(len(kiwi_service.find_routes('C', 'C', max_distance=30)))