import re

class KiwilandRailroadService(object):
    """
    Manage route information for the Kiwiland Railroad service. Allows 
    a Kiwiland agent to find the distance of specific routes, number of
    available routes between two locations, and find the shortest route
    to a location.
    """

    MAX_FIND_DISTANCE = 50 # when trying to find the shortest route

    def __init__(self):
        self.route_map = {}


    class NoSuchRouteException(Exception):
        """
        Exception raised when a specific route does not exist.
        """
        pass


    class Town(object):
        """
        Define a town simply by its name and the available routes to 
        another destination. The value of the route is the distance to 
        that destination.
        """
        def __init__(self, name):
            self.name = name
            self.routes = {}

        def add_route(self, destination, distance):
            self.routes[destination] = distance

        def find_route(self, destination):
            return self.routes[destination]


    def add_routes(self, route_graph):
        """
        Add routes for each route code in `route_graph`. Each route is added
        to the unidirectional map `self.route_map`.

        If an invalid route code is present in `route_graph` a generic 
        Exception is raised and no more routes are added to the route map.
        """
        for route_code in route_graph:
            # Extract route elements from route_code and validate. Must be alpha-alpha-digit(s).
            route_match = re.match(r'^(\w)(\w)(\d+)$', route_code)
            if not route_match:
                raise Exception('Invalid route identifier "{}"'.format(route_code))
            
            # Add route for route code 'AZn', where first letter is source town,
            # second is destination, and 'n' is distance
            town, destination, distance = route_match.groups()
            self.route_map.setdefault(town.upper(), self.Town(town.upper()))\
                .add_route(destination.upper(), int(distance))

        return self

    def get_route_distance(self, route_spec):
        """
        Get the route distance for `route-spec` which is a hyphen delimited
        string of location names ('A-B-C'). 

        If the route does not exist, a NoSuchRouteException is raised.
        """
        stops = [t.upper() for t in route_spec.split('-')]
        distance = 0

        for start,destination in zip(stops, stops[1:]):
            try:
                start_town = self.route_map[start]
                distance += start_town.find_route(destination)
            except KeyError:
                raise self.NoSuchRouteException('No such route: {}'.format(route_spec))
        return distance

    def _traverse_route(self, location, end, max_stops=0, breadcrumbs='', distance=0, 
            stop_num=0, max_distance=0, exact_stops=False):
        """
        Traverses a route by recursing through each route available for the location.
        Return a list of routes along with the distance for each route (as a tuple) if
        the route is less than `max_stops` or, if `exact_stops` is True, the route is 
        exactly `max_stops`.

        Alternatively, if `max_distance` is specified, the traversal will terminate once
        the accumulated distance for a route exceeds `max_distance`. In this case, 
        `max_stops` is ignored.

        The parameters `breadcrumbs`, `distance`, and 'stop_num' are used to keep track 
        of the route taken ('AEBC', etc.), the actual distance for the route, and the
        current stop number for the location.
        """
        breadcrumbs = breadcrumbs + location
        trips = []

        if max_distance and distance < max_distance or stop_num < max_stops:
            # If we've reached the destination prior to reaching the maximum
            # number of stops, save the route
            if location == end and not exact_stops and stop_num:
                trips.append((breadcrumbs, distance))

            # Traverse each route in the current location
            for dest,dist in self.route_map[location].routes.items():
                trips += self._traverse_route(dest, end, max_stops, breadcrumbs, distance+dist,
                    stop_num+1, max_distance, exact_stops)
        else:
            # We've reached the end of max_stops or max_distance, so check that location
            # matches the final destination and save the route.
            if location == end and (distance < max_distance or max_distance == 0):
                trips.append((breadcrumbs, distance))

        return trips

    def get_trip_count(self, start, end, max_stops, exact_stops=False):
        """
        Return the number of trips from `start` location to the `end` location, not
        exceeding `max_stops`. If `exact_stops` is True, only match trips that are
        the exact number of stops at `max_stops`.
        """
        trips = self._traverse_route(start, end, max_stops, exact_stops=exact_stops)
        return len(trips)

    def find_routes(self, start, end, max_distance):
        """
        Find all routes from `start` location to `end` location that do not exceed
        the maximum distance of `max_distance`. Return a list of tuples for each matched
        route, containing the route specification ('ABCD') and distance (int), sorted
        by ascending distance.
        """
        trips = self._traverse_route(start, end, max_distance=max_distance)
        return sorted(trips, key=lambda x: x[1])

    def find_shortest_route(self, start, end):
        """
        Find the shortest route from `start` location to `end` location and return
        the distance of this route. If not routes are found, return None.
        """
        routes = self.find_routes(start, end, self.MAX_FIND_DISTANCE)
        return routes[0][1] if routes else None

